package com.code.with.bisky.exception;

public interface ErrorCode {
    String CD_NOT_FOUND_1 = "CD_NOT_FOUND_1"; // user does not exist
    String CD_NOT_FOUND_2 = "CD_NOT_FOUND_2";
}
