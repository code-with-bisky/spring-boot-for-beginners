package com.code.with.bisky.repository;

import com.code.with.bisky.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,String> {
}
