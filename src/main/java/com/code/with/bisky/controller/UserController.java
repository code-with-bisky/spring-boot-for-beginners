package com.code.with.bisky.controller;

import com.code.with.bisky.dto.UserUpdateRecord;
import com.code.with.bisky.exception.NotFoundException;
import com.code.with.bisky.model.User;
import com.code.with.bisky.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {


    private final UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/{id}",produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User>  getUserById(@PathVariable  String id){


        User user1 = userService.getUserById(id);

        if(user1 == null){
            throw new NotFoundException(String.format("User with id %s does not exist",id));
        }

        return  ResponseEntity.ok(user1);
    }

    @DeleteMapping(value = "/{id}",produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User>  deleteUserById(@PathVariable  String id){


        userService.deleteUserById(id);

        return  ResponseEntity.status(HttpStatus.OK).build();
    }


    @Operation(summary = "Creates a new user",
            description = "REST Endpoint that creates a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created user"),
            @ApiResponse(responseCode = "400", description = "Bad request"),
            @ApiResponse(responseCode = "500", description = "Server Error")
    })
    @PostMapping
    public ResponseEntity<User>  createUser(@RequestBody  User user){




        return  ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(user));
    }


    @PutMapping ("/{id}")
    public ResponseEntity<UserUpdateRecord>  updateUser(@PathVariable  String id, @RequestBody  UserUpdateRecord userUpdateRecord){


        return  ResponseEntity.status(HttpStatus.CREATED).body(userService.updateUser(id,userUpdateRecord));
    }





    @GetMapping(value = "/search-by-first-name",produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<User>>  getUsersByFirstname(@RequestParam  String firstName,@RequestHeader("X-API-KEY") String apiKey){


        return  ResponseEntity.ok(userService.findUsersByFirstName(firstName));
    }


}
