package com.code.with.bisky.service.user;

import com.code.with.bisky.dto.UserUpdateRecord;
import com.code.with.bisky.exception.NotFoundException;
import com.code.with.bisky.model.User;
import com.code.with.bisky.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserById(String id) {

        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteUserById(String id) {


           /*
            first find the record by id
            If it is exists we then delete the record
             */

        User user1 = getUserById(id);
        if(user1 == null){
            throw new NotFoundException(String.format("User with id %s does not exist",id));
        }
        // logic to delete
        userRepository.delete(user1);


    }

    @Override
    public User createUser(User user) {
          /*
          check if username exists
          throw an exception that the user already exists

          or create a new user
         */
        return userRepository.save(user);
    }

    @Override
    public UserUpdateRecord updateUser(String id, UserUpdateRecord userUpdateRecord) {
           /*
            first find the record by id
            If it is exists we then update the record
             */

        User user = getUserById(id);
        if(user == null){
            throw new NotFoundException(String.format("User with id %s does not exist",id));
        }
        user.setFirstName(userUpdateRecord.firstName());
        user.setLastName(userUpdateRecord.lastName());
        user.setAge(userUpdateRecord.age());

        User saved = userRepository.save(user);

        return new UserUpdateRecord(
                saved.getFirstName(),
                saved.getLastName(),
                saved.getAge(),
                saved.getId());
    }

    @Override
    public List<User> findUsersByFirstName(String firstName) {
  /*
            first find the record by id

             */

        return getUsers().stream().filter(user -> user.getFirstName().equalsIgnoreCase(firstName)).collect(Collectors.toList());
    }


    private User demoUser(String id,String username,String firstName,String lastName,int age){

        return User.builder()
                .id(id)
                .username(username)
                .firstName(firstName)
                .lastName(lastName)
                .age(age)
                .roles( Arrays.asList("DEVELOPER","MAINTAINER","ADMIN"))
                .build();
    }


    private List<User> getUsers(){

        List<User>  users=new ArrayList<>();
        users.add(demoUser("1","bisky","Bisky","Mursuid",30));
        users.add(demoUser("2","jwhite","John","White",35));
        users.add(demoUser("3","pwalker","Paul","Walker",32));
        users.add(demoUser("4","jsnow","John","Snow",40));

        return users;
    }
}
