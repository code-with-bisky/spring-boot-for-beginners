package com.code.with.bisky.service.user;

import com.code.with.bisky.dto.UserUpdateRecord;
import com.code.with.bisky.model.User;

import java.util.List;

public interface UserService {

    User getUserById(String id);
    void  deleteUserById(String id);

    User createUser(User user);
    UserUpdateRecord updateUser(String id, UserUpdateRecord user);

    List<User> findUsersByFirstName(String firstName);
}
