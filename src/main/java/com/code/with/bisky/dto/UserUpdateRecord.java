package com.code.with.bisky.dto;

public record UserUpdateRecord (String firstName,String lastName,int age, String id){
}
